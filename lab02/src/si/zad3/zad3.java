package si.zad3;

public class zad3 {

    static int MAX = Integer.MAX_VALUE;
    static int MIN = Integer.MIN_VALUE;

    public static void main (String[] args)
    {
        int values[] = {3,12,8,2,4,6,14,5,2};
        int optimal = maximin(0, 0, false, values, MIN, MAX);
        System.out.println("The optimal value for MAX is : " + optimal);

    }
    public static int maximin(int depth, int index, Boolean maximizing, int values[], int alpha, int beta) {
        if (depth == 2) {
            return values[index];
        }
        if (maximizing) {
            int optimal = MIN;
            for (int i = 0; i < 3; i++) {
                int val = maximin(depth + 1, index * 3 + i, false, values, alpha, beta);
                optimal = Math.max(optimal, val);
                alpha = Math.max(alpha, optimal);
                if (beta <= alpha)
                    break;
            }
            return optimal;
        }
        else
        {
            int optimal = MAX;
            for (int i = 0; i < 3; i++) {
                int val = maximin(depth + 1, index * 3 + i, true, values, alpha, beta);
                optimal = Math.min(optimal, val);
                beta = Math.min(beta, optimal);
                if (beta <= alpha)
                    break;
            }
            return optimal;
        }
    }
}