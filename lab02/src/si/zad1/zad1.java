package si.zad1;

import java.util.ArrayList;
import java.util.LinkedList;

public class zad1 {
    public static void main(String[] args) {

        state goal1 = new state(new Boolean[]{false,true}, new Boolean[]{false,false});
        state goal2 = new state(new Boolean[]{true,false}, new Boolean[]{false,false});

        ArrayList<state> goal = new ArrayList<>();
        goal.add(goal1);
        goal.add(goal2);

        node A = new node(new state(new Boolean[]{true,false}, new Boolean[]{true,true}));
        node B = new node(new state(new Boolean[]{false,true}, new Boolean[]{true,true}));
        node C = new node(new state(new Boolean[]{true,false}, new Boolean[]{false,true}));
        node D = new node(new state(new Boolean[]{false,true}, new Boolean[]{true,false}));
        node E = new node(new state(new Boolean[]{false,true}, new Boolean[]{false,true}));
        node F = new node(new state(new Boolean[]{true,false}, new Boolean[]{true,false}));
        node G = new node(goal2);
        node H = new node(goal1);

        A.addNeighbor(C);
        A.addNeighbor(B);
        B.addNeighbor(A);
        B.addNeighbor(D);
        C.addNeighbor(E);
        E.addNeighbor(C);
        E.addNeighbor(G);
        D.addNeighbor(F);
        F.addNeighbor(H);
        F.addNeighbor(D);
        H.addNeighbor(G);
        G.addNeighbor(H);

        ArrayList<node> bfs = bfs(goal, A);
        display(bfs);

    }
    public static ArrayList<node> bfs(ArrayList<state> goals, node start) {
        LinkedList<node> queue = new LinkedList<>();
        ArrayList<node> visited = new ArrayList<>();
        queue.add(start);

        node temp;
        ArrayList<node> output = new ArrayList<>();

        while(!queue.isEmpty()) {
            temp = queue.removeLast();
            output.add(temp);
            boolean isGoal = false;
            for(state i : goals) {
                if(temp.getData().equals(i)) {
                    isGoal = true;
                }
            }

            if(isGoal) {
                return output;
            } else {
                visited.add(temp);
                for(node i : temp.getNeighbors()) {
                    queue.addFirst(i);
                }
                for(node i : visited) {
                    queue.remove(i);
                }
            }
        }
        return null;
    }

    public static void display(ArrayList<node> bfs) {
        for(int i = bfs.size()-2; i >= 0; --i) {
            boolean isNeighbor = false;
            for(node j : bfs.get(i).getNeighbors()) {
                for(int k = i+1; k < bfs.size(); ++k) {
                    if(j.getData().equals(bfs.get(k).getData()))
                        isNeighbor = true;
                }
            }
            if(!isNeighbor) {
                bfs.remove(bfs.get(i));
                i = bfs.size()-2;
            }
        }
        for(int i = 0; i < bfs.size()-1; ++i) {
            bfs.get(i).getData().wypisz();
            if(bfs.get(i).getData().action()[2]) {
                System.out.println("ACTION : Vacuum");
                System.out.println("--------------------------");
            } else {
                if(bfs.get(i).getData().action()[1]) {
                    System.out.println("ACTION : Move Right");
                    System.out.println("--------------------------");
                } else {
                    System.out.println("ACTION : Move Left");
                    System.out.println("--------------------------");
                }
            }
        }
        bfs.get(bfs.size()-1).getData().wypisz();
    }
}
