package si.zad1;

import java.util.ArrayList;

public class node {

    private state data;
    private ArrayList<node> neighbors;

    public node(state data) {
        this.data = data;
        neighbors = new ArrayList<>();
    }

    public void addNeighbor(node node) {
        this.neighbors.add(node);
    }

    public state getData() {
        return data;
    }

    public ArrayList<node> getNeighbors() {
        return neighbors;
    }

}