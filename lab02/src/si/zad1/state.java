package si.zad1;

public class state {
    Boolean [] lokalizacja;
    Boolean [] czyBrudne;

    public state(Boolean [] lokalizacja, Boolean [] czyBrudne){
        this.lokalizacja = lokalizacja;
        this.czyBrudne = czyBrudne;
    }

    public void moveLeft(){
        if (!this.lokalizacja[0]){
            this.lokalizacja[0] = true;
            this.lokalizacja[1] = false;
        }
    }

    public void moveRight(){
        if (this.lokalizacja[0]){
            this.lokalizacja[0] = false;
            this.lokalizacja[1] = true;
        }
    }

    public void clean(){
        if (this.lokalizacja[0]){
            this.czyBrudne[0]=false;
        }
        else{
            this.czyBrudne[1]=false;
        }
    }

    public Boolean [] action(){
        //[moveLeft, moveRight, clean]
        Boolean [] possibleActions = new Boolean[3];
        possibleActions[0] = false;
        possibleActions[1] = false;
        possibleActions[2] = false;

        if (this.lokalizacja [0]){
            possibleActions[1] = true;
            if (this.czyBrudne[0]){
                possibleActions[2] = true;
            }
        }
        else if (this.lokalizacja [1]){
            possibleActions[0] = true;
            if (this.czyBrudne[1]){
                possibleActions[2] = true;
            }
        }
        return possibleActions;
    }

    public state result(int action){
        //0 - moveLeft, 1 - moveRight, 2 - clean
        state result = new state(this.lokalizacja, this.czyBrudne);
        if (action == 0){
            result.moveLeft();
        }
        else if (action == 1){
            result.moveRight();
        }
        else result.clean();
        return result;
    }

    public void wypisz(){
        if (this.lokalizacja[0]){
            System.out.println("lokalizacja: pokój A");
        }
        else {
            System.out.println("lokalizacja: pokój B");
        }
        if (!this.czyBrudne[0]){
            System.out.println("Pokój A stan: czyste");
        }
        else{
            System.out.println("Pokój A stan: brudne");
        }
        if (!this.czyBrudne[1]){
            System.out.println("Pokój B stan: czyste");
        }
        else{
            System.out.println("Pokój B stan: brudne");
        }
        System.out.println("--------------------------");
    }

}
